<?php
  $msg = "";
  if(isset($_POST['submit'])){
    $default = $_POST;
    $name=$_POST['name'];
    $dob=$_POST['dob'];
    $address=$_POST['address'];
    $email=$_POST['email'];
    $contact=$_POST['contact'];
    $bgroup=$_POST['bgroup'];
    echo "<script type='text/javascript'>".$bgroup."</script>";
    $con = mysqli_connect("localhost","root","","bloodbank");
    if(!$con)
      die("Can't connect to the database");
    $sql="INSERT INTO donor(name,dob,address,email,contact,b_group) VALUES('".$name."','".$dob."','".$address."','".$email."','".$contact."','".$bgroup."')";
    $result=mysqli_query($con,$sql);
    if($result==0){
      $msg="Error adding donor! Please check the fields.";
    }
    else{
        $msg="Donor entry successful!";
        }
        echo "<script type='text/javascript'>alert('".$msg."');</script>";
    }
  
?>
<html>
  <head>
    <title>Add Donors</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <script type="text/javascript"></script>
      <style type="text/css">
        form{
          margin: 40px;
      border-radius:80px;
      padding: 30px;
      width: 40%;
      overflow: none;
      background-color: #E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4);         }
        input.date{
          color: grey;
          width: 100%;
          text-align: center;
        }
        input.add{
        background-color: #bf3939;
        box-shadow: none;
        color: white;
      }
        input{
      text-align: center;
      font-family: Nunito;
      background-color:#E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4); 
        border:1px solid transparent;
      border-radius: 20px;
      padding: 10px;
      margin: 15px;
    }
    input.login{
        background-color: #E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4);         padding: 10px;
        border: 0px solid transparent;
        width: 40%;
        color: #fff;
        background-color: #e64b40;
        border-color: #E0E5EC;
        font-size: .8rem;
        padding: .75rem 1rem;
        transition: transform .5s;
      }
    input.login:hover{
      cursor: pointer;
      transform: scale(1.2);
    }
    input:focus{
      outline: none;
    }
    td{
      padding: 10px;
    }
      </style>
  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
      <div id="content" class="p-4 p-md-5 pt-5">
      <h2 class="mb-4">Enter the details</h2>
      <center>
      <form method="post" action="<?=$_SERVER['PHP_SELF'];?>" name="theForm">
        <table>
          <tr>
            <td>Full Name:</td>
            <td><input type="text" name="name" placeholder="Enter name here"></td>
          </tr>
          <tr>
            <td>Date of Birth:</td>
            <td><input type="date" name="dob" ></td>
          </tr>
          <tr>
            <td>Address:</td>
            <td><input type="text" name="address" placeholder="Enter address here"></td>
          </tr>
          <tr>
            <td>Email:</td>
            <td><input type="email" name="email" placeholder="Enter email here"></td>
          </tr>
          <tr>
            <td>Contact:</td>
            <td><input type="text" name="contact" placeholder="Enter contact here"></td>
          </tr>
          <tr>
            <td>Blood Group:</td>
            <td>
              <SELECT name="bgroup">
                <option value=1>A+</option>
                <option value=2>A-</option>
                <option value=3>B+</option>
                <option value=4>B-</option>
                <option value=5>AB+</option>
                <option value=6>AB-</option>
                <option value=7>O+</option>
                <option value=8>O-</option>
              </SELECT>
            </td>
          </tr>
          <tr>
            <td colspan=2><input class="add" type="submit" name="submit" value="Add Donor"></td>
          </tr>
        </table>
      </form>
      
     </div>
    </div>
    <div class="msg" >
      
     
    </div>


</body>
<script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

     
</html>
