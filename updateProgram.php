<html>
  <head>
    <title>View Donation Programs</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <style type="text/css">
      body{
        background-color: #E0E5EC;
        font-family: Nunito;
      }
        .input-fields{
          width: 50%;
          padding: 4%;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border-radius: 50px;
          background-color: #E0E5EC;  
          margin-left: 0.2%;
          margin-top: 7%;
        }
        table{
          border-collapse: collapse;
          border-radius: 20px;
        }
        td{
          text-align: center;
          padding: 7px;
          width: auto;
        }
        input{
          font-family: Nunito;
          border:1px solid transparent;
          background-color: #E0E5EC;
          outline: none;
          color: grey;
          border-radius: 20px;
          text-align: center;
        }
        input.find{
          background-color:#d32f2f;
          color:white;
          transition: transform .5s;
          outline: none;
        }
        .TFtable{
    width:30%; 
    text-align: center;
    border-collapse:collapse; 
    border-color: transparent;
  }
  .TFtable td{ 
    padding:7px;
  }
  /* provide some minimal visual accomodation for IE8 and below */
  .TFtable tr{
    background: #E0E5EC;
  }
  /*  Define the background color for all the ODD background rows  */
  .TFtable tr:nth-child(odd){ 
    background: #E0E5EC;
  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable tr:nth-child(even){
    background: #e64b40;
    color: white;
  }
        input.find:hover{
          cursor: pointer;
          transform: scale(1.2);
        }
        h4{
          font-family: Lobster Two;
          font-weight: bold;
          color:#d32f2f;
        }
      </style>
  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
           <div id="content" class="p-4 p-md-5 pt-5">
<center>
<div class="wrapper">
  <div class="contact-form">
    <div class="input-fields">
      <center>
        <h4>Update the details</h4>
        <table class="TFtable" border="1px" bordercolor="ghostwhite">
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Date</td>
            <td>Address</td>
            <td>Action</td>
          </tr>
          <?php
              $conn = mysqli_connect("localhost", "root", "", "bloodbank");
              // Check connection
              if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
              }
              $sql = "SELECT id, name, date,address FROM program";
              $result = $conn->query($sql);
              if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                  echo "<form method='POST' action='updateP.php'> <input type='hidden'  value='" . $row["id"]. "' name='id'>
                  <tr><td>" . $row["id"]. "</td>
                        <td>" . $row["name"]. "</td>
                        <td>" . $row["date"]."</td>
                        <td>" .  $row["address"]. "</td>
                        ";
                 
                        echo"<td>";
              // echo"
              //  <a href='update.php?id=".$row["id"]."'>";
                  echo "<input type=\"submit\" name=\"update\" class=\"find\" value=\"Update\"/>
                                 </form><br><br>
                                <form method='POST' action='deleteProgram.php'> 
                                <input type='hidden'  value='" . $row["id"]. "' name='id'><input type=\"submit\" class=\"find\" value=\"Delete\"/>
                                </form></td></tr>";
                  }}
               else { echo "0 results"; }

              


              $conn->close();
              ?>
        </table>
      </center>
     </div>
    </div>
    <div class="msg" >
      
     
    </div>
  </div>
</div>  
</div>

</body>
           </div>
          <!-- Page Content  -->

          <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</html>
