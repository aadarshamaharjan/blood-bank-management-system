<?php
  $con = mysqli_connect("localhost","root","","bloodbank");
  if(!$con)
    die("Can't connect to the database");
  $stockSQL="SELECT * FROM blood";
  $topDonorSQL="SELECT name, SUM(volume) as volume FROM blood_donor  as bd INNER JOIN blood as b INNER JOIN donor as d WHERE bd.bid=b.id AND bd.did=d.id GROUP BY bd.did ORDER BY bd.volume DESC LIMIT 5";
  $bgProvidedSQL="SELECT SUM(volume),bgroup FROM blood_donor AS bd INNER JOIN blood AS b WHERE bd.bid=b.id GROUP BY bd.bid";
  $bgReceivedSQL="SELECT SUM(volume),bgroup FROM blood_hospital AS bh INNER JOIN blood AS b WHERE bh.bid=b.id GROUP BY bh.bid";
  $topProgramSQL="SELECT p.name AS name, SUM(volume) FROM program as p INNER JOIN blood_donor as bd WHERE p.id=bd.received_at GROUP BY p.name ORDER BY volume DESC LIMIT 5";
  $forecastDonor="SELECT SUM(volume)/4 as volume,date FROM blood_donor HAVING DATE(date)>=DATE(NOW())-INTERVAL 4 MONTH";
  $forecastHospital="SELECT SUM(volume)/4 as volume,created_at FROM blood_hospital HAVING DATE(created_at)>=DATE(NOW())-INTERVAL 4 MONTH";
?>
<!doctype html>
<html lang="en">
  <head>
    	<title>Dashboard</title>
    	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	  <link rel="stylesheet" href="dashboard.css">
      <link rel="stylesheet" href="datacharts/dashboard.css">
      
<script src="datacharts\canvasjs-2.3.2\canvasjs.min.js"></script>
  </head>
  <body>
  
		<div class="wrapper d-flex align-items-stretch">
			<?php include('navbar.php');?>
        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 pt-5">
        <h2 class="mb-4">Dashboard</h2>
        <?php include("modal.php"); if($msg!=""){echo("<p class='alertbox'><center><h2 class='alertbox'>ALERT!</h2> The stock is too low for blood group ".$msg.". Blood Donation Camp can be held soon!!!</center></p>");}?>
        <div class="maincontainer">
        <div  id="stock" class="stock">Here Stock</div>
        <div id="forecast" class="forecast"><?php include('datacharts/forecast.php'); ?></div>
        <div id="topDonor"></div>
          <div id="twoColumn"></div>
          <div id="topProgram"></div>
          
        <div>

        </div>
        </div>
      </div>
		</div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <?php include('datacharts/topDonor.php'); ?>
    <?php include('datacharts/stock.php'); ?>
<?php include('datacharts/twoColumn.php'); ?>
    <?php include('datacharts/topProgram.php'); ?>
  </body>
</html>