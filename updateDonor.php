<html>
  <head>
    <title>Alert Donors</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <style type="text/css">
      body{
        background-color: #E0E5EC;
        font-family: Nunito;
      }
        .input-fields{
          border-radius: 50px;
          background-color: #E0E5EC;  
          margin-left: 0.2%;
          margin-top: 7%;
        }
        table{
          padding: 10px;
        background-color:#E0E5EC;
        box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4); 
        border:1px solid transparent;
        }

        td{
          text-align: center;
          padding: 1vh 1.5vw;
          width: auto;
          }

        input{

        }
        input.find{
          background-color:#d32f2f;
          color:white;
          transition: transform .5s;
          outline: none;
        }
        input.find:hover{
          cursor: pointer;
          transform: scale(1.2);
        }
        h4{
          font-family: Lobster Two;
          font-weight: bold;
          color:#d32f2f;
        }

 

  .TFtable tr{
    background: #b8d1f3;
  }
  /*  Define the background color for all the ODD background rows  */
  .TFtable tr:nth-child(odd){ 
    background: #d32f2f;
    color: white;
  }.TFtable tr:nth-child(odd) input{ 
              font-family: Nunito;
          border:1px solid transparent;
          background-color: #E0E5EC;
          outline: none;
          color: grey;
          border-radius: 20px;
          text-align: center;
  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable tr:nth-child(even){
    background: #dae5f4;
  }
  .TFtable tr:nth-child(even) input{
              font-family: Nunito;
          border:1px solid transparent;
          background-color: #d32f2f;
          outline: none;
          color: white;
          border-radius: 20px;
          text-align: center;

  }


      </style>
  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
           <div id="content" class="p-4 p-md-5 pt-5">

<div class="wrapper">
  <div class="contact-form">
    <div class="input-fields">
      <center>
        <h4>Update the details</h4>
        <table border="1px" class="TFtable">
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Date of Birth</td>
            <td>Address</td>
            <td>Email</td>
            <td>Contact</td>
            <td>Blood Group</td>
            <td>Action</td>
          </tr>
          <?php
              $conn = mysqli_connect("localhost", "root", "", "bloodbank");
              // Check connection
              if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
              }
              $sql = "SELECT id, name, dob,address,email,contact,b_group FROM donor";
              $result = $conn->query($sql);
              if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                  echo "<form method='POST' action='update.php'> <input type='hidden'  value='" . $row["id"]. "' name='id'>
                  <tr><td>" . $row["id"]. "</td>
                        <td>" . $row["name"]. "</td>
                        <td>" . $row["dob"]."</td>
                        <td>" .  $row["address"]. "</td>
                        <td>" . $row["email"] . "</td>
                        <td>" . $row["contact"]. "</td>";
                        $i=$row["b_group"];
                  $sql1 = "SELECT bgroup FROM blood WHERE id='$i'";
                  $result1 = $conn->query($sql1);
                  if ($result1->num_rows > 0) {  
                    while($row1 = $result1->fetch_assoc()) {
                        echo"
                        <td>" .$row1["bgroup"]."</td>";
                    }
                  }
                        echo"<td>";
              // echo"
              //  <a href='update.php?id=".$row["id"]."'>";
                  echo "<input type=\"submit\" name=\"update\" class=\"find\" value=\"Update\"/>
                                 </form><br><br>
                                <form method='POST' action='delete.php'> 
                                <input type='hidden'  value='" . $row["id"]. "' name='id'><input type=\"submit\" class=\"find\" value=\"Delete\"/>
                                </form></td></tr>";


              }
              } else { echo "0 results"; }

              


              $conn->close();
              ?>
        </table>
      </center>
     </div>
    </div>
    <div class="msg" >
      
     
    </div>
  </div>
</div>  
</div>

</body>
           </div>
          <!-- Page Content  -->

          <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</html>
