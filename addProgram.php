<?php
  $msg = "";
  if(isset($_POST['submit'])){
    $default = $_POST;
    $name=$_POST['name'];
    $date=$_POST['date'];
    $address=$_POST['address'];
    $con = mysqli_connect("localhost","root","","bloodbank");
    if(!$con)
      die("Can't connect to the database");
    $sql="INSERT INTO program(name,date,address) VALUES('".$name."','".$date."','".$address."')";
    $result=mysqli_query($con,$sql);
    if($result==0){
      $msg="Error adding program! Please check the fields.";
    }
    else{
        $msg="Program entry successful!";
        }
        echo "<script type='text/javascript'>alert('".$msg."');</script>";
    }
  
?>
<html>
  <head>
    <title>Add Programs</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <script type="text/javascript"></script>
          <style type="text/css">
        form{
          margin: 40px;
      border-radius:80px;
      padding: 30px;
      width: 30%;
      overflow: none;
      background-color: #E0E5EC;
      box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px rgba(255,255,255, 0.5);
        }
        input{
      text-align: center;
      font-family: Nunito;
      background-color:#E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4); 
        border:1px solid transparent;
      border-radius: 20px;
      padding: 10px;
      margin: 15px;
    }
    input.date{
      color: grey;
    }
    textarea{
      text-align: center;
      font-family: Nunito;
      background-color:#E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4); 
        border:1px solid transparent;
      border-radius: 20px;
      padding: 10px;
      margin: 15px;
      color: grey;

    }
    textarea:focus{
      outline: none;
    }
    input.login{
        background-color: #E0E5EC;
        box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px rgba(255,255,255, 0.5);
        padding: 10px;
        border: 0px solid transparent;
        width: 50%;
        color: #fff;
        background-color: #e64b40;
        border-color: #E0E5EC;
        font-size: .8rem;
        padding: .75rem 1rem;
        transition: transform .5s;
      }
    input.login:hover{
      cursor: pointer;
      transform: scale(1.2);
    }
    input:focus{
      outline: none;
    }
    td{
      padding: 10px;
      text-align: center;
    }
      </style>

  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
      <div id="content" class="p-4 p-md-5 pt-5">
      <h2 class="mb-4">Enter the details of program</h2>
      <center>
      <form method="post" action="<?=$_SERVER['PHP_SELF'];?>" name="theForm">
        <table>
          <tr>
            <td><input type="text" name="name" placeholder="Enter name here"></td>
          </tr>
          <tr>
            <td><input type="date" class="date" name="date" ></td>
          </tr>
          <tr>
            <td><input type="text" name="address" placeholder="Enter address here"></td>
          </tr>
          <tr>
            <td colspan=2><input type="submit" class="login" name="submit" value="Add Program"></td>
          </tr>
        </table>
      </form>
      
     </div>
    </div>
    <div class="msg" >
      
     
    </div>


</body>
<script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

     
</html>
