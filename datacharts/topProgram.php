<?php
    $results=mysqli_query($con,$topProgramSQL);
    $datapoints = array();
    while ($row = $results->fetch_assoc()) {
        $name = $row['name'];
        $volume = $row['SUM(volume)'];
       array_push($datapoints, array("y"=> $volume,"name"=> "$name"));
   }
?>

<script>
var chart2 = new CanvasJS.Chart("topProgram", {
	theme: "light2",
	exportFileName: "Top Programs",
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "Top Programs"
	},
	legend:{
		cursor: "pointer",
		itemclick: explodePie
	},
	data: [{
		type: "doughnut",
		innerRadius: 30,
		showInLegend: true,
		toolTipContent: "<b>{name}</b>: {y} units",
		indexLabel: "{name} - #percent%",
		dataPoints: <?php echo json_encode($datapoints, JSON_NUMERIC_CHECK); ?>
	}]
});
function explodePie (e) {
	if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
	} else {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
	}
	e.chart2.render();
}
chart2.render();
</script>
