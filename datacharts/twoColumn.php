<?php
	$result=mysqli_query($con,$bgProvidedSQL);
	$result1=mysqli_query($con,$bgReceivedSQL);
	$datapoints = array();
	$datapoints1=array();
    while ($row = $result->fetch_assoc()) {
         $provided = $row['SUM(volume)'];
         $bg = $row['bgroup'];
        array_push($datapoints, array("y"=> $provided,"label"=> "$bg"));
	}
	while ($row1 = $result1->fetch_assoc()) {
		$received = $row1['SUM(volume)'];
		$bg = $row1['bgroup'];
	   array_push($datapoints1, array("y"=> $received,"label"=> "$bg"));
   }
?>

<script>

var chart = new CanvasJS.Chart("twoColumn", {
	animationEnabled: true,
	title:{
		text: "Blood Received/Provided Chart"
	},	
	axisY: {
		title: "Received",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisY2: {
		title: "Provided",
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E"
	},	
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Received",
		legendText: "Total Received From Donors",
		showInLegend: true, 
		dataPoints: <?php echo json_encode($datapoints, JSON_NUMERIC_CHECK); ?>
	},
	{
		type: "column",	
		name: "Provided",
		legendText: "Total Provided To Hospital",
		axisYType: "secondary",
		showInLegend: true,
		dataPoints: <?php echo json_encode($datapoints1, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

</script>
</head>
<body>
