<?php
    $result=mysqli_query($con,$stockSQL);
	$datapoint = array();
        while ($row = $result->fetch_assoc()) {
             $bg = $row['bgroup'];
             $totalblood = $row['totalblood'];
            array_push($datapoint, array("y"=> $totalblood,"label"=> "$bg"));
        }
?>
<script>
 
var chart1 = new CanvasJS.Chart("stock", {
	animationEnabled: true,
	exportEnabled: true,
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	title:{
		text: "BLOOD STOCK"
	},
	data: [{
        type: "column", //change type to bar, line, area, pie, etc  
		dataPoints: <?php echo json_encode($datapoint, JSON_NUMERIC_CHECK); ?>
	}]
});
 chart1.render();
</script>


