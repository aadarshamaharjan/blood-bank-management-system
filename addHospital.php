<?php
  $msg = "";
  if(isset($_POST['submit'])){
    $default = $_POST;
    $name=$_POST['name'];
    $address=$_POST['address'];
    $contact=$_POST['contact'];
    $email=$_POST['email'];
    $website=$_POST['website'];
    $con = mysqli_connect("localhost","root","","bloodbank");
    if(!$con)
      die("Can't connect to the database");
    $sql="INSERT INTO hospital(name,address,contact,email,website) VALUES('".$name."','".$address."','".$contact."','".$email."','".$website."')";
    $result=mysqli_query($con,$sql);
    if($result==0){
      $msg="Error adding program! Please check the fields.";
    }
    else{
        $msg="Program entry successful!";
        }
        echo "<script type='text/javascript'>alert('".$msg."');</script>";
    }
  
?>
<html>
  <head>
    <title>Add Hospital</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <script type="text/javascript"></script>
      <style type="text/css">
      input.add{
        background-color: #bf3939;
        box-shadow: none;
        color: white;
      }
    form{
      margin: 40px;
      border-radius:80px;
      padding: 30px;
      width: 33%;
      overflow: none;
      background-color: #E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4);     }
    body{
      background-color: #E0E5EC;
      font-family: Nunito;
    }
    input{
      text-align: center;
      font-family: Nunito;
      background-color:#E0E5EC;
      box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
        -9px -9px 9px rgba(255, 255, 255, 0.6),
        inset 2px 2px 3px rgba(0, 0, 0, 0.03),
        inset -2px -2px 3px rgba(255, 255, 255, 0.4); 
        border:1px solid transparent;
      border-radius: 50px;
      padding: 2vh;
      margin: 15px;
    }
    input.login{
        background-color: #E0E5EC;
        box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px rgba(255,255,255, 0.5);
        padding: 10px;
        border: 0px solid transparent;
        width: 50%;
        color: #fff;
        background-color: #e64b40;
        border-color: #E0E5EC;
        font-size: .8rem;
        transition: transform .5s;
      }
    input.login:hover{
      cursor: pointer;
      transform: scale(1.2);
    }
    td{
      padding: 2vh 2vw;
    }
    input:focus{
      outline: none;
    }
  </style>
  </head>
  <body>
<div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>

          <!-- Page Content  -->
          <div id="content" class="p-4 p-md-5 pt-5">
      <h2 class="mb-4">Enter the details of program</h2>
      <center>
      <form method="post" action="<?=$_SERVER['PHP_SELF'];?>" name="theForm">
        <table>
          <tr>
            <td><input type="text" name="name" placeholder="Enter name here"></td>
          </tr>
          <tr>
            <td><input type="text" name="address"  placeholder="Enter address here" ></td>
          </tr>
          <tr>
            <td><input type="text" name="contact" placeholder="Enter contact here"></td>
          </tr>
          <tr>
            <td><input type="text" name="email" placeholder="Enter email here"></td>
          </tr>
          <tr>
            <td><input type="text" name="website" placeholder="Enter website here"></td>
          </tr>
          <tr>
            <td colspan=2 align="center"><input class="login" type="submit" name="submit" value="Add Hospital"></td>
          </tr>
        </table>
      </form>
      
     </div>
    </div>
    <div class="msg" >
      
     
    </div>


</body>
<script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

     
</html>
