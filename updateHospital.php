<html>
  <head>
    <title>Update Hospitals </title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <style type="text/css">
      body{
        background-color: #E0E5EC;
        font-family: Nunito;
      }
        .input-fields{
          width: 100%;
          padding: 4%;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border-radius: 50px;
          background-color: #E0E5EC;  
          margin-left: 0.2%;
          margin-top: 7%;
        }
        table{
          border-collapse: collapse;
          border-radius: 20px;
        }
        td{
          text-align: center;
          padding: 2px;
          width: auto;
        }
        input{
          font-family: Nunito;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border:1px solid transparent;
          background-color: #E0E5EC;
          outline: none;
          color: grey;
          border-radius: 20px;
          text-align: center;
        }
        input.find{
          background-color:#d32f2f;
          color:white;
          transition: transform .5s;
          outline: none;
        }
        input.find:hover{
          cursor: pointer;
          transform: scale(1.2);
        }
        h4{
          font-family: Lobster Two;
          font-weight: bold;
          color:#d32f2f;
        }
      </style>
  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
           <div id="content" class="p-4 p-md-5 pt-5">

<div class="wrapper">
  <div class="contact-form">
    <div class="input-fields">
      <center>
        <h4>Update the details</h4>
        <table border="1px" bordercolor="ghostwhite">
          <tr>
            <td>S.N</td>
            <td>Name</td>
            <td>Address</td>
            <td>Contact</td>
            <td>Website</td>
            <td>Action</td>
          </tr>

<?php 
 $conn = mysqli_connect("localhost", "root", "", "bloodbank");
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
} 
$idd=$_POST['id'];
$sql = "SELECT id, name, address,contact,email FROM hospital";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
while($row = $result->fetch_assoc()) {
 
  if($row["id"]==$idd){
    echo "<form method='POST' action='updateHBackend.php'>
    <input type='hidden'  value='" . $row["id"]. "' name='id'>
    <tr>
          <td>" . $row["id"]. "</td>
          <td><input type='text' value='" . $row["name"]. "' name='name'></td>
          <td><input type='text' value='" . $row["address"]. "' name='address'></td>          
          <td><input type='text' value='" . $row["contact"]. "' name='email'></td>
          <td><input type='text' value='" . $row["email"]. "' name='contact'></td>
         ";
       
    echo "</select></td><td><input type=\"submit\"  class=\"find\" value=\"Save\"/><br><br></form>
    <form method='POST' action='deleteH.php'> 
    <input type='hidden'  value='" . $row["id"]. "' name='id'><input type=\"submit\" class=\"find\" value=\"Delete\"/>
    </form> </td></tr>";
  }
  else{

    echo "<form method='POST' action='update.php'><tr><td>" . $row["id"]. "</td>
              <td>" . $row["name"]. "</td>
              <td>"
    . $row["address"]."</td>
    <td>" .  $row["contact"]. "</td>
    <td>" . $row["email"] . "</td>
    ";
  
    echo"
     <input type='hidden'  value='" . $row["id"]. "' name='id'>";
    // echo"
    //  <a href='update.php?id=".$row["id"]."'>";
     echo "<td><input type=\"submit\" name='update' class=\"find\" value=\"Update\"/><br><br>
     <a href=\"delete.php\"><input type=\"button\" class=\"find\" value=\"delete\"/></a></td></tr>";
  }
  echo "</form>";
}

}

 


$conn->close();
?>
        </table>
      </center>
     </div>
    </div>
    <div class="msg" >
      
     
    </div>
  </div>
</div>  
</div>

</body>
           </div>
          <!-- Page Content  -->

     
</html>