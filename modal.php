<?php
    $result=mysqli_query($con,$stockSQL);
    $msg="";
	$datapoint = array();
        while ($row = $result->fetch_assoc()) {
             $bg = $row['bgroup'];
             $totalblood = $row['totalblood'];
            array_push($datapoint, array("y"=> $totalblood,"label"=> "$bg"));
        }
?>

<?php
	foreach ($datapoint as $data){
        if($data['y']<5){
            $msg=$msg." ".$data['label'].", ";
            echo"<div></div>";
        }
  } 
?>

<!-- The Modal -->
<div id="myModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <?php if($msg!=""){echo("<p class='alerttext'><center><h2 class='alerttext'>ALERT!</h2> The stock is too low for blood group ".$msg.". Blood Donation Camp can be held soon!!!</center></p>");}
    else{echo("<p class='alerttext'><center><h2 class='alerttext'>Welcome!</h2> The stock is maintained.</center></p>");}
    ?>
  </div>

</div>
<style>
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.6); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 15% auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  margin-left: 98%;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
.alerttext{
    color: red;
    align: center;
}
</style>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
// btn.onclick = function() {
//   modal.style.display = "block";
// }

modal.style.display = "block";
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>