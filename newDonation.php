<?php
  $contact = "";
  $ErrorMsg="";
  $flag=1;
  if(isset($_POST['submit'])){
  	$contact=$_POST['pnum'];
  	$received_at=$_POST['program'];
	  $volume=$_POST["volume"];
	  $date=$_POST["date"];
    $con = mysqli_connect("localhost","root","","bloodbank");
	if(!$con)
		die("Can't connect to the database");
	$sql="SELECT * FROM donor WHERE contact='".$contact."'";
	$result=mysqli_query($con,$sql);
	if(mysqli_num_rows($result)!=0){
		$row = mysqli_fetch_assoc($result);
		$bid=$row["b_group"];
		$did=$row["id"];
	}
	else{
		$flag=0;
		$ErrorMsg="Cannot fetch donor information from the provided number! DONOR not REGISTERD!!";
	}
	if($flag==1){
		$sql="INSERT INTO blood_donor(bid, did, volume, received_at, date) VALUES (".$bid.", ".$did.", '".$volume."', $received_at, '".$date."')";
		if(!mysqli_query($con,$sql)){
			$flag=0;
			$ErrorMsg="Cannot insert provided data in the database. Please review the data provided.";
		}
		if($flag==1){
			$sql="UPDATE blood SET totalblood=totalblood+".$volume." WHERE id=".$bid;
			if(!mysqli_query($con,$sql)){
				$flag=0;
				$ErrorMsg="Cannot update the blood table in database.";
			}
			else
				$ErrorMsg="Donation entry successful!";
		}
	}

	echo "<script type='text/javascript'>alert('$ErrorMsg');</script>";
	
  }
  
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>New Donations</title>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />-->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
  <!-- Bootstrap Css -->
  <!--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">-->
  <!-- Script -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 
<!-- jQuery UI -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 
<!-- Bootstrap Css -->
<!--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">-->

		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
  		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  		<link rel="stylesheet" href="dashboard.css">
  			<style type="text/css">
  	body{
  		font-family: Nunito;
  	}
  		form{
  			margin: 40px;
			border-radius:80px;
			padding: 20px;
			width: 60%;
			overflow: none;
			background-color: #E0E5EC;
box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
		    -9px -9px 9px rgba(255, 255, 255, 0.6),
		    inset 2px 2px 3px rgba(0, 0, 0, 0.03),
		    inset -2px -2px 3px rgba(255, 255, 255, 0.4);	  		}
  		input{
			text-align: center;
			font-family: Nunito;
			background-color:#E0E5EC;
			box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
		    -9px -9px 9px rgba(255, 255, 255, 0.6),
		    inset 2px 2px 3px rgba(0, 0, 0, 0.03),
		    inset -2px -2px 3px rgba(255, 255, 255, 0.4);	
		    border:1px solid transparent;
			border-radius: 20px;
			padding: 10px;
			margin: 15px;width: 50%;
		}
		select{
			text-align: center;
			font-family: Nunito;
			background-color:#E0E5EC;
			box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
		    -9px -9px 9px rgba(255, 255, 255, 0.6),
		    inset 2px 2px 3px rgba(0, 0, 0, 0.03),
		    inset -2px -2px 3px rgba(255, 255, 255, 0.4);	
		    border:1px solid transparent;
			border-radius: 20px;
			padding: 10px;
			margin: 15px;width: 50%;
			color: grey;
		}
		select:focus{
			outline: none;
		}
		input.add{
  			background-color: #bf3939;
  			box-shadow: none;
  			color: white;
  		}
		input.date{
			color: grey;
			text-align: center;
		}
		input.vol{
			width: 96%;
			color: grey;
		}
		input.login{
		    background-color: #E0E5EC;
		    padding: 10px;
		    width:30%;
		    color: #fff;
		    background-color: #e64b40;
		    border-color: #E0E5EC;
		    font-size: .8rem;
		    padding: .75rem 1rem;
		    transition: transform .5s;
			}
		input.login:hover{
			cursor: pointer;
			transform: scale(1.2);
		}
		input:focus{
			outline: none;
		}
		td{
			padding: 1.7vh;
			text-align: center;
		}
  	</style>

  		


	</head>
	<body>
		<div class="wrapper d-flex align-items-stretch">
			<?php include('navbar.php');?>

	        <!-- Page Content  -->
	      	<div id="content" class="p-4 p-md-5 pt-5">
	      		<h2 class="mb-4">New Donation</h2>
	      		<div class="formcontent">
	      			<center>
	      			<form method="post" action="<?=$_SERVER['PHP_SELF'];?>" onsubmit="return validate(this)" name="theForm">
	      				<table>
	      					<!-- <tr>
	      						<td colspan="2">Note: Please register donor if phone number doesn't show up! &nbsp Also register program if not registerd!!</td>
	      					</tr> -->
	      					<tr><td>&nbsp</td></tr>
	      					<tr>
	      						<td><input type="text" size="30" name="pnum" id="pnum" placeholder="Phone of Donor" onKeyUp="charAlert()"></td>
	      					</tr>
	      					<tr>
	      						<td><input type="text" size="50" name="dname" id="dname" placeholder="Name of Donor"></td>
	      					</tr>	      					
	      					<tr>
	      						<td><input class="date" type="date" size="50" name="date"></td>
	      					</tr>
	      					<tr>
	      						<td><input type="number" size="50" name="volume" placeholder="in units" value="1"></td>
	      					</tr>
	      					<tr>
	      						<td>
	      							<?php 
	      								$conn = new mysqli("localhost", "root", "", "bloodbank") or die ('Cannot connect to db');
									    $result = $conn->query("Select name,id from program GROUP BY name");
									    echo "<html>";
									    echo "<body>";
									    echo "<select name='program'>";
									    while ($row = $result->fetch_assoc()) {
											$pname = $row['name'];
											$pid = $row['id'];
									        echo '<option value=" '.$pid.'"  >'.$pname.'</option>';
									    }
									    echo "</select>";
									    echo "</body>";
									    echo "</html>";
									?>
								</td>
	      					</tr>
	      					<tr>
	      						<td align="center" colspan="2"><input class="login" type="submit" value="Add Donation" name="submit"><br/></td>
	      					</tr>
	      				</table>				
					</form>
					<br/>
	      		</div>
	      	</div>
			</div>
			<script type="text/javascript">
			  $(function() {
			     $( "#dname" ).autocomplete({
			       source: 'search/ajax-db-search.php',
			     });
			  });
			  $(function() {
			     $( "#pnum" ).autocomplete({
			       source: 'search/phone.php',
			     });
			  });


			  function charAlert() {
				var textField = document.theForm.pnum;

				if(textField.value.length > 9){
				/* must be > 5, not == 5 or else the substring statement on the next line will cause the function to run again if the user dismisses the alert by pressing the 'enter' key rather than clicking 'OK'. */

				textField.value= textField.value.substring(0,10);
				// set field's value equal to first five characters.

				textField.blur();
				/* move cursor out of form element to keep it from placing itself at position zero, causing an overwrite of the first character */
				// $.ajax({
			 //     url: 'autocomplete.php',
			 //     type: 'post',
			 //     data: {userid:textField},
			 //     dataType: 'json',
			 //     success:function(response){
			 
			 //      var len = response.length;

			 //      if(len > 0){
			 //       // var id = response[0]['id'];
			 //       var name = response[0]['name'];
			 //       // var email = response[0]['email'];
			 //       // var age = response[0]['age'];
			 //       // var salary = response[0]['salary'];

			 //       // Set value to textboxes
			 //       document.getElementById('dname').value = name;
			 //       // document.getElementById('age_'+index).value = age;
			 //       // document.getElementById('email_'+index).value = email;
			 //       // document.getElementById('salary_'+index).value = salary;
			 
			 //      }
			 
			 //     }
			 //    });
				}

				}
				
			</script>
    <!-- <script src="js/jquery.min.js"></script> -->
    <!-- <script src="js/popper.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>