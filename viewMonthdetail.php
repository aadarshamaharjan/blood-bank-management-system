<?php
      $con = mysqli_connect("localhost","root","","bloodbank");
    if(!$con) die("Can't connect to the database");  
    if(isset($_POST['submit'])){
      $default = $_POST;
      $selectedYear=$_POST['selectedYear'];
      $selectedMonth=$_POST['selectedMonth'];
    }
    else{
      $selectedYear=date("Y");
      $selectedMonth=date("m");
    }
    
  $dateObj   = DateTime::createFromFormat('!m', $selectedMonth);
  $monthName = $dateObj->format('F');
?>
<html>
  <head>
    <title>Monthly Report</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
<script src="datacharts\canvasjs-2.3.2\canvasjs.min.js"></script>
      
      <style type="text/css">
      body{
        background-color: #E0E5EC;
        font-family: Nunito;
      }
        .input-fields{
          width: 100%;
          padding: 4%;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border-radius: 50px;
          background-color: #E0E5EC;  
          margin-left: 0.2%;
          margin-top: 7%;
        }
        table{
          border-collapse: collapse;
          border-radius: 20px;
        }
        td{
          text-align: center;
          padding: 7px;
          width: auto;
        }
        input{
          font-family: Nunito;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border:1px solid transparent;
          background-color: #E0E5EC;
          outline: none;
          color: grey;
          border-radius: 20px;
          text-align: center;
        }
        input.find{
          background-color:#d32f2f;
          color:white;
          transition: transform .5s;
          outline: none;
        }
        input.find:hover{
          cursor: pointer;
          transform: scale(1.2);
        }
        h4{
          font-family: Lobster Two;
          font-weight: bold;
          color:#d32f2f;
        }

  .TFtable{
    width:30%; 
    text-align: center;
    border-collapse:collapse; 
    border-color: transparent;
  }
  .TFtable td{ 
    padding:7px;
  }
  /* provide some minimal visual accomodation for IE8 and below */
  .TFtable tr{
    background: #E0E5EC;
  }
  /*  Define the background color for all the ODD background rows  */
  .TFtable tr:nth-child(odd){ 
    background: #E0E5EC;
  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable tr:nth-child(even){
    background: #e64b40;
    color: white;
  }
.TFtable2{
    width:100%; 
        border-color: transparent;
    border-collapse:collapse; 
  }
  .TFtable2 td{ 
  }
  /* provide some minimal visual accomodation for IE8 and below */
  .TFtable2 tr{
    background: #b8d1f3;
  }
  /*  Define the background color for all the ODD background rows  */
  .TFtable2 tr:nth-child(odd){ 
    background: #E0E5EC;
  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable2 tr:nth-child(even){
    background: #e64b40;
    color: white;
  }

  .TFtable3{
    width:100%; 
        border-color: transparent;
    border-collapse:collapse; 
  }
  .TFtable3 td{ 
  }
  /* provide some minimal visual accomodation for IE8 and below */
  .TFtable3 tr{
    background: #b8d1f3;
  }
  /*  Define the background color for all the ODD background rows  */
  .TFtable3 tr:nth-child(odd){ 
    background: #E0E5EC;
  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable3 tr:nth-child(even){
    background: #e64b40;
    color: white;
  }
  #bloodprovided{
    height: 10%;
    width: 15%;

}
#bloodreceived{
    height: 10%;
    width: 15%;

}

      </style>
      <script type="text/javascript"></script>
  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
      <div id="content" class="p-4 p-md-5 pt-5">
      <h2 class="mb-4"><?php echo($selectedYear.", ".$monthName);?>'s Report</h2>
      <span class="date">
        <form method="post" action="<?=$_SERVER['PHP_SELF'];?>" name="theForm">
        <input type="number" min="1900" max="2099" step="1" value="<?php echo date("Y"); ?>" placeholder="Enter Year" name="selectedYear">
        <input type="number" min="1" max="12" step="1" value="<?php echo date("m"); ?>" placeholder="Enter Month" name="selectedMonth">
        <input type="submit" value="Generate" name="submit">
        </form>
      </span>
    
      <table class="TFtable" border=1><caption>This Month's Total Blood Received Report</caption><tr><th>Blood Group</th><th>Volume</th></tr>
<!-- <?php       
  $d=date('Y/m');
  $sql="SELECT id,bgroup FROM blood";
  $result=mysqli_query($con,$sql);
  if(mysqli_num_rows($result)>0){
      while($row=mysqli_fetch_assoc($result)){
        $j=0;
        $i = $row['id'];
        echo "<tr><td>".$row['bgroup']."</td>";                    
        $sql1="SELECT id,SUM(volume) as v,created_at as c FROM blood_donor WHERE bid='$i'";
        $result1=mysqli_query($con,$sql1);
        if($result1){
          if(mysqli_num_rows($result1)>0){ 
            while($row1=mysqli_fetch_assoc($result1)){
              $dates=$row1['c'];
              $yr=new DateTime($dates);
              $yr->format('Y/m');                                    
              if($d==$yr->format('Y/m')){
                $j+=$row1['v'];
              }
            }
          } 
        }
        echo "<td>".$j."</td></tr>";
      }
    }
?> -->

<?php       
  $d=date('Y/m');
  $datapoint=array();
  $sql="SELECT id,bgroup FROM blood";
  $result=mysqli_query($con,$sql);
  if(mysqli_num_rows($result)>0){
      while($row=mysqli_fetch_assoc($result)){
        $j=0;
        $i = $row['id'];
        echo "<tr><td>".$row['bgroup']."</td>";                    
        $sql1="SELECT id,SUM(volume) as v,created_at as c FROM blood_donor WHERE bid='$i' AND YEAR(date)=".$selectedYear." AND MONTH(date)=".$selectedMonth."";
        $result1=mysqli_query($con,$sql1);
        if($result1){
          if(mysqli_num_rows($result1)>0){ 
            while($row1=mysqli_fetch_assoc($result1)){
              $j+=$row1['v'];
              }
            }
          } 
          
        array_push($datapoint, array("y"=> $j,"label"=> $row['bgroup']));
        echo "<td>".$j."</td></tr>";
      }
    }
?>
</table>
<div id="bloodreceived"><?php include("datacharts/totalbloodreceivedchart.php");?></div>

<table class="TFtable2" border=1><caption>This Month's Donor's Donation Report</caption><tr><th>S.No.</th><th>Name</th><th>Blood Group</th><th>Program Name</th><th>Received Date</th></tr>
<?php       
  $d=date('Y/m');
  $i=1;
  $sql="SELECT p.name AS pname, d.name AS name, bgroup, bd.date AS created_at FROM blood_donor AS bd INNER JOIN donor as d INNER JOIN blood AS b INNER JOIN program AS p WHERE bd.bid=b.id AND bd.received_at=p.id AND bd.did=d.id AND YEAR(bd.date)=".$selectedYear." AND MONTH(bd.date)=".$selectedMonth."";
  $result=mysqli_query($con,$sql);
  if(mysqli_num_rows($result)>0){
      while($row=mysqli_fetch_assoc($result)){
        $name = $row['name'];
        $bgroup = $row['bgroup'];
        $pname = $row['pname'];
        $dates = $row['created_at'];
                echo "<tr><td>".$i++."</td><td>".$name."</td><td>".$bgroup."</td><td>".$pname."</td><td>".$dates."</td></tr>";
            }
          } 
        
?>
            
      </table>
      <br/><br/>
        
      <table class="TFtable3" border=1><caption>This Month's Donation Program Report</caption><tr><th>S.No.</th><th>Program Name</th><th>Address</th><th>Total Blood Received</th><th>Date</th></tr>
<?php       
  $d=date('Y/m');
  $i=1;
  $sql="SELECT name,address,SUM(volume) as volume,p.date as date FROM program as p INNER JOIN blood_donor as bd WHERE p.id=bd.received_at AND YEAR(bd.date)=".$selectedYear." AND MONTH(bd.date)=".$selectedMonth." GROUP BY p.id";
  $result=mysqli_query($con,$sql);
  if(mysqli_num_rows($result)>0){
      while($row=mysqli_fetch_assoc($result)){
        $name = $row['name'];
        $address = $row['address'];
        $volume = $row['volume'];
        $dates = $row['date'];
        $yr=new DateTime($dates);
          
                echo "<tr><td>".$i++."</td><td>".$name."</td><td>".$address."</td><td>".$volume."</td><td>".$yr->format('Y-m-d')."</td></tr>";
              
            }
          } 
        
?>
<div id="bloodprovided"></div>
<table class="TFtable3" border=1><caption>This Month's Blood Provided Report</caption><tr><th>S.No.</th><th>Hospital Name</th><th>Total Blood Provided</th><th>Date</th></tr>
<?php       
  $d=date('Y/m');
  $i=1;
  $datapoint=array();
  $sql="SELECT name,SUM(volume) as volume,bh.created_at as date FROM blood_hospital as bh INNER JOIN hospital as h WHERE h.id=bh.hid AND YEAR(bh.created_at)=".$selectedYear." AND MONTH(bh.created_at)=".$selectedMonth." GROUP BY h.id";
  $result=mysqli_query($con,$sql);
  if(mysqli_num_rows($result)>0){
      while($row=mysqli_fetch_assoc($result)){
        $name = $row['name'];
        $volume = $row['volume'];
        $dates = $row['date'];        
        $yr=new DateTime($dates);
                echo "<tr><td>".$i++."</td><td>".$name."</td><td>".$volume."</td><td>".$yr->format('Y-m-d')."</td></tr>";
                
        array_push($datapoint, array("y"=> $volume,"label"=> $name));
              }
            
          } 
        
?>
<?php include("datacharts/totalbloodprovidedchart.php");?>
            
      </table>
            <br/><br/><br/><br/>

        
      </div>
    </div>
  </body>
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
</html>