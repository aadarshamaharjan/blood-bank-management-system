<html>
  <head>
    <title>Alert Donors</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="dashboard.css">
      <style type="text/css">
      body{
        background-color: #E0E5EC;
        font-family: Nunito;
      }
        .input-fields{
          width: 100%;
          padding: 4%;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border-radius: 50px;
          background-color: #E0E5EC;  
          margin-left: 0.2%;
          margin-top: 7%;
        }
        table{
          border-collapse: collapse;
          border-radius: 20px;
        }
        td{
          text-align: center;
          padding: 2px;
          width: auto;
        }
        input{
          font-family: Nunito;
          box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px    rgba(255,255,255, 0.5);
          border:1px solid transparent;
          background-color: #E0E5EC;
          outline: none;
          color: grey;
          border-radius: 20px;
          text-align: center;
        }
        input.find{
          background-color:#d32f2f;
          color:white;
          transition: transform .5s;
          outline: none;
        }
        input.find:hover{
          cursor: pointer;
          transform: scale(1.2);
        }
        h4{
          font-family: Lobster Two;
          font-weight: bold;
          color:#d32f2f;
        }
      </style>
  </head>
  <body>
    <div class="wrapper d-flex align-items-stretch">
      <?php include('navbar.php');?>
           <div id="content" class="p-4 p-md-5 pt-5">

<div class="wrapper">
  <div class="contact-form">
    <div class="input-fields">
      <center>
        <h4>Update the details</h4>
        <table border="1px" bordercolor="ghostwhite">
          <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Date of Birth</td>
            <td>Address</td>
            <td>Email</td>
            <td>Contact</td>
            <td>Blood Group</td>
            <td>Action</td>
          </tr>

<?php 
 $conn = mysqli_connect("localhost", "root", "", "bloodbank");
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}


 $idd=$_POST['id'];
    $sql = "SELECT id, name, dob,address,email,contact,b_group FROM donor";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
  if($row["id"]==$idd){
    echo "<form method='POST' action='updatebackend.php'>
    <input type='hidden'  value='" . $row["id"]. "' name='id'>
    <tr>
          <td>" . $row["id"]. "</td>
          <td><input type='text' value='" . $row["name"]. "' name='name'></td>
          <td><input type='text' value='" . $row["dob"]. "' name='dob'></td>
          <td><input type='text' value='" . $row["address"]. "' name='address'></td>          
          <td><input type='text' value='" . $row["email"]. "' name='email'></td>
          <td><input type='text' value='" . $row["contact"]. "' name='contact'></td>
          <td><select name='b_group'>";

    $sql1 = "SELECT id,bgroup FROM blood";
    $result1 = $conn->query($sql1);
    if ($result1->num_rows > 0) {  
      while($row1 = $result1->fetch_assoc()) {
          echo"
          <option value='".$row1["id"]."'>".$row1["bgroup"]."</option>";
      }
    }

       
    echo "</select></td><td><input type=\"submit\"  class=\"find\" value=\"Save\"/><br><br>
    <form method='POST' action='delete.php'> 
    <input type='hidden'  value='" . $row["id"]. "' name='id'><input type=\"submit\" class=\"find\" value=\"Delete\"/>
    </form> </td></tr>";
  }
  else{

    echo "<form method='POST' action='update.php'><tr><td>" . $row["id"]. "</td>
              <td>" . $row["name"]. "</td>
              <td>"
    . $row["dob"]."</td>
    <td>" .  $row["address"]. "</td>
    <td>" . $row["email"] . "</td>
    <td>"
    . $row["contact"]. "</td>";
$i=$row["b_group"];
    $sql1 = "SELECT bgroup FROM blood WHERE id='$i'";
    $result1 = $conn->query($sql1);
    if ($result1->num_rows > 0) {  
      while($row1 = $result1->fetch_assoc()) {
          echo"
          <td>" .$row1["bgroup"]."</td>";
      }
    }
  
    echo"
     <input type='hidden'  value='" . $row["id"]. "' name='id'>";
    // echo"
    //  <a href='update.php?id=".$row["id"]."'>";
     echo "<td><input type=\"submit\" name='update' class=\"find\" value=\"Update\"/><br><br>
     <a href=\"delete.php\"><input type=\"button\" class=\"find\" value=\"delete\"/></a></td></tr>";
  }
  echo "</form>";
}

}

 


$conn->close();
?>
        </table>
      </center>
     </div>
    </div>
    <div class="msg" >
      
     
    </div>
  </div>
</div>  
</div>

</body>
           </div>
          <!-- Page Content  -->

     
</html>



<!-- <!DOCTYPE html>
<html>
<head>
 <title></title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>

 <div class="col-lg-6 m-auto">
 
 <form method="post">
 
 <br><br><div class="card">
 
 <div class="card-header bg-dark">
 <h1 class="text-white text-center">  Update Operation </h1>
 </div><br>

 
 <label>Name: </label>
 <input type="text" name="name" class="form-control" value="<?php  $n ?>"> <br>

  <label>DOB: </label>
 <input type="text" name="dob" class="form-control" value="<?php  $d ?>"> <br>

  <label>Address: </label>
 <input type="text" name="address" class="form-control" value="<?php  $a ?>"> <br>

  <label>Email: </label>
 <input type="text" name="email" class="form-control"value="<?php  $e ?>"> <br>

  <label>Contact: </label>
 <input type="text" name="contact" class="form-control"value="<?php  $c ?>"> <br>
  <label>BGROUP: </label>
 <input type="text" name="b_group" class="form-control"value="<?php  $b ?>"> <br>

 <button class="btn btn-success" type="submit" name="done"> Submit </button><br>

 </div>
 </form>
 </div>
</body>
</html> -->