<?php
  $pname = $address = "";
  $message = $passErr = "";$ErrorMsg="";
  if(isset($_POST['submit'])){
  	$pname=$_POST['pname'];
	$address=$_POST['address'];
	$date=$_POST['date'];
	$message=$_POST['message'];	
    $con = mysqli_connect("localhost","root","","bloodbank");
	if(!$con)
		die("Can't connect to the database");
	$sql="SELECT * FROM donor";
	$result=mysqli_query($con,$sql);
	while ($row = $result->fetch_assoc()) {
		$email=$row["email"];
		require "sendEmail.php";
	}
	echo "<script type='text/javascript'>alert('$ErrorMsg');</script>";
	
  }
  
?>
<html>
	<head>
		<title>Alert Donors</title>
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
  		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  		<link rel="stylesheet" href="dashboard.css">
  		<style type="text/css">
  			form{
      		margin: 40px;
			border-radius:80px;
			padding: 30px;
			width: 60%;
			overflow: none;
			background-color: #E0E5EC;
			box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px rgba(255,255,255, 0.5);
  			}
  			input{
			text-align: center;
			font-family: Nunito;
			background-color:#E0E5EC;
			box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
		    -9px -9px 9px rgba(255, 255, 255, 0.6),
		    inset 2px 2px 3px rgba(0, 0, 0, 0.03),
		    inset -2px -2px 3px rgba(255, 255, 255, 0.4);	
		    border:1px solid transparent;
			border-radius: 20px;
			padding: 10px;
			margin: 15px;
		}
		input.date{
			color: grey;
		}
		textarea{
			text-align: center;
			font-family: Nunito;
			background-color:#E0E5EC;
			box-shadow: 9px 9px 9px rgba(0, 0, 0, 0.05),
		    -9px -9px 9px rgba(255, 255, 255, 0.6),
		    inset 2px 2px 3px rgba(0, 0, 0, 0.03),
		    inset -2px -2px 3px rgba(255, 255, 255, 0.4);	
		    border:1px solid transparent;
			border-radius: 20px;
			padding: 10px;
			margin: 15px;
			color: grey;

		}
		textarea:focus{
			outline: none;
		}
		input.login{
		    background-color: #E0E5EC;
		    box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px rgba(255,255,255, 0.5);
		    padding: 10px;
		    border: 0px solid transparent;
		    width: 20%;
		    color: #fff;
		    background-color: #e64b40;
		    border-color: #E0E5EC;
		    font-size: .8rem;
		    padding: .75rem 1rem;
		    transition: transform .5s;
			}
		input.login:hover{
			cursor: pointer;
			transform: scale(1.2);
		}
		input:focus{
			outline: none;
		}
		td{
			padding: 10px;
		}
  		</style>

	</head>
	<body>
		<div class="wrapper d-flex align-items-stretch">
			<?php include('navbar.php');?>

	        <!-- Page Content  -->
	      	<div id="content" class="p-4 p-md-5 pt-5">
	      		<h2 class="mb-4">Alert Donors</h2>
	      		<div class="formcontent">
	      			<center>
	      			<form method="post" action="<?=$_SERVER['PHP_SELF'];?>" onsubmit="return validate(this)" name="theForm">
	      				<table>
	      					<tr>
	      						<td>Program Name:</td>
	      						<td><input type="text" size="50" name="pname"></td>
	      					</tr>
	      					<tr>
	      						<td>Date:</td>
	      						<td><input class="date" type="date" size="50" name="date"></td>
	      					</tr>
	      					<tr>
	      						<td>Address:</td>
	      						<td><input type="text" size="50" name="address"></td>
	      					</tr>
	      					<tr>
	      						<td>Message:</td>
	      						<td><textarea rows="10" cols="54" name="message">This is to inform all the registered donors that a blood donation program is going to be held. Please allocate your time for the donation if possible. Help save a LIFE! </textarea></td>
	      					</tr>
	      					<tr>
	      						<td colspan="2" align="center"><input class="login" type="submit" value="Alert" name="submit"><br/></td>
	      					</tr>
	      				</table>				
					</form>
					<br/>
	      		</div>
	      	</div>
			</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>