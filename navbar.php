<?php
  session_start();
  if(!isset($_SESSION['user'])){
    header("Location: index.php");
  }
?>

<!doctype html>
<html lang="en">
  <head>
    	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	  <link rel="stylesheet" href="dashboard.css">
  </head>
<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	          <i class="fa fa-bars"></i>
	          <span class="sr-only">Toggle Menu</span>
	        </button>
        </div>
				<div class="p-4 pt-5">
		  		<a href="dashboard.php"><img src="img/BBMS.png" height="50px" width="40px" style="margin-left: 42%; margin-bottom: 40px;"></a>
          <p align="center"><b>BBMS</b></p>
	        <ul class="list-unstyled components mb-5">
	          <li class="active">
	            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Manage Blood</a>
	            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li>
                    <a href="newDonation.php">New Donation Received Entry</a>
                </li>
                <li>
                    <a href="blood_hospital.php">Blood Provided to Hospital Entry</a>
                </li>
	            </ul>
	          </li>
	       
	          <li>
              <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Manage Donor</a>
              <ul class="collapse list-unstyled" id="pageSubmenu">
                <li>
                    <a href="adddonor.php">Add New Donor</a>
                </li>
                <li>
                    <a href="updateDonor.php">View Donor Infomation</a>
                </li>
              </ul>
	          </li>
			  <li>
        <li>
              <a href="#pageSubmenuss" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Manage Hospital</a>
              <ul class="collapse list-unstyled" id="pageSubmenuss">
                <li>
                    <a href="addhospital.php">Add New Hospital Information</a>
                </li>
                <li>
                    <a href="updateH.php">View Hospital Infomation</a>
                </li>
              </ul>
	          </li>
			  <li>
              <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Manage Programs</a>
              <ul class="collapse list-unstyled" id="pageSubmenu1">
                <li>
                    <a href="addProgram.php">Add New Program</a>
                </li>
                <li>
                    <a href="updateProgram.php">Update Program Infomation</a>
                </li>
              </ul>
	          </li>
	          <li>
              <a href="viewMonthdetail.php">View This Month's Report</a>
	          </li>
	          <li>
              <a href="campAlert.php">Alert Donors</a>
	          </li>
            <li>
              <a href="logout.php">Logout</a>
	          </li>
	        </ul>	    

	        <div class="footer">
	        	
	        </div>

	      </div>
    	</nav>