-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2020 at 04:32 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bloodbank`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `created_at`) VALUES
(1, 'admin', 'YWRtaW4=', '2019-12-27 14:01:14');

-- --------------------------------------------------------

--
-- Table structure for table `blood`
--

CREATE TABLE `blood` (
  `id` int(11) NOT NULL,
  `bgroup` varchar(25) NOT NULL,
  `totalblood` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood`
--

INSERT INTO `blood` (`id`, `bgroup`, `totalblood`) VALUES
(1, 'A+', 1),
(2, 'A-', 0),
(3, 'B+', 0),
(4, 'B-', 0),
(5, 'AB+', 0),
(6, 'AB-', 0),
(7, 'O+', 1),
(8, 'O-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blood_donor`
--

CREATE TABLE `blood_donor` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `bid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `volume` int(11) NOT NULL,
  `received_at` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_donor`
--

INSERT INTO `blood_donor` (`id`, `created_at`, `bid`, `did`, `volume`, `received_at`, `date`) VALUES
(23, '2020-02-12 12:51:11', 1, 1, 1, 2, '2020-02-12'),
(24, '2020-02-12 12:51:34', 1, 1, 1, 3, '2020-01-16'),
(25, '2020-02-12 13:17:45', 7, 3, 1, 2, '2019-12-30');

-- --------------------------------------------------------

--
-- Table structure for table `blood_hospital`
--

CREATE TABLE `blood_hospital` (
  `id` int(11) NOT NULL,
  `hid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `volume` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_hospital`
--

INSERT INTO `blood_hospital` (`id`, `hid`, `bid`, `created_at`, `volume`) VALUES
(9, 1, 1, '2020-02-12 13:30:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE `donor` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `b_group` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donor`
--

INSERT INTO `donor` (`id`, `name`, `dob`, `address`, `email`, `contact`, `b_group`, `created_at`) VALUES
(1, 'Aadarsha Maharjan', '1998-06-25', 'Nardevi, Kathmandu', 'aadarshamaharjan@gmail.com', '9843356744', 1, '2020-02-11 15:10:11'),
(2, 'Sumedh Bajracharya', '1998-02-19', 'New Road, Kathmandu', 'sumedhbajracharya07@gmail.com', '9860104311', 1, '2020-01-18 16:34:35'),
(3, 'Sonu Karmacharya', '1998-10-08', 'Dallu, Kathmandu', 'sonu444.karmacharya@gmail.com', '9861845592', 7, '2020-01-18 16:34:35'),
(10, 'Sona Karmacharya', '1998-09-06', 'Thamel,kathmandu', 'sona.karmacharya@gmail.com', '9818267244', 1, '2020-02-12 06:30:41'),
(11, 'Sujata Khadka', '1999-02-04', 'Ason', 'suzu@gmail.com', '9845120365', 4, '2020-02-12 06:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE `hospital` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`id`, `name`, `address`, `contact`, `email`, `website`) VALUES
(1, 'Bir Hospital', 'Kanti Path, Kathmandu', '01-4221119', 'admin@nams.org.np', 'http://www.nams.org.np/'),
(2, 'Ayurveda Hospital', 'Nardevi, Kathmandu', '01-4259182', 'N/A', 'https://www.facebook.com/pages/Nardevi-Ayurvedic-Hospital/148786021992615'),
(3, 'Kathmandu Model Hospital', 'Adwait Marg, Kathmandu', '01-4240806', 'kamhson@wlink.com.np', 'https://www.facebook.com/pages/Kathmandu-Model-Hospital/291173404752180'),
(4, 'Norvic International Hospital', 'Thapathali, Kathmandu', '01-4258554', 'info@norvichospital.com', 'https://norvichospital.com/'),
(5, 'Nepal Cancer Hospital and Research Centre', 'Satdobato, Lalitpur ', '01-5251312', 'info@nch.com.np', 'http://www.nch.com.np/'),
(6, 'Patan Hospital', 'Lagankhel Satdobato Rd, Lalitpur', '01-5522295', 'info@patanhospital.org.np', 'http://www.pahs.edu.np/pahs-community/hospital/'),
(9, 'Adarsa Hospital', 'Dallu', '016212042', 'adarsahospital@gail.com', 'www.adarsa.com.np');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `login_info` int(11) DEFAULT NULL,
  `logout_info` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `name`, `date`, `address`) VALUES
(2, 'Direct Donation', '2019-12-01', 'NaN'),
(3, 'ABC Donation Camp', '2020-02-11', 'New Road, Kathmandu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blood`
--
ALTER TABLE `blood`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blood_donor`
--
ALTER TABLE `blood_donor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bid` (`bid`),
  ADD KEY `did` (`did`),
  ADD KEY `received_at` (`received_at`);

--
-- Indexes for table `blood_hospital`
--
ALTER TABLE `blood_hospital`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bid` (`bid`),
  ADD KEY `hid` (`hid`);

--
-- Indexes for table `donor`
--
ALTER TABLE `donor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_group` (`b_group`);

--
-- Indexes for table `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood`
--
ALTER TABLE `blood`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `blood_donor`
--
ALTER TABLE `blood_donor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `blood_hospital`
--
ALTER TABLE `blood_hospital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `donor`
--
ALTER TABLE `donor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `hospital`
--
ALTER TABLE `hospital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blood_donor`
--
ALTER TABLE `blood_donor`
  ADD CONSTRAINT `blood_donor_ibfk_1` FOREIGN KEY (`bid`) REFERENCES `blood` (`id`),
  ADD CONSTRAINT `blood_donor_ibfk_2` FOREIGN KEY (`did`) REFERENCES `donor` (`id`),
  ADD CONSTRAINT `blood_donor_ibfk_3` FOREIGN KEY (`received_at`) REFERENCES `program` (`id`);

--
-- Constraints for table `blood_hospital`
--
ALTER TABLE `blood_hospital`
  ADD CONSTRAINT `blood_hospital_ibfk_1` FOREIGN KEY (`bid`) REFERENCES `blood` (`id`),
  ADD CONSTRAINT `blood_hospital_ibfk_2` FOREIGN KEY (`hid`) REFERENCES `hospital` (`id`);

--
-- Constraints for table `donor`
--
ALTER TABLE `donor`
  ADD CONSTRAINT `donor_ibfk_1` FOREIGN KEY (`b_group`) REFERENCES `blood` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
